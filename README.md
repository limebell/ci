# README

The repository is devoted to the CI-MAI (Computational Intelligence - Master in
Artificial Intelligence) course. The author are:

  + Salvador Medina Herrera
  + Carles Miralles Pena
  + Alejandro Suárez Hernández

## Project: Biped walk with evolutionary algorithms

The following is an extract of the project's proposal. The full preliminary
description can be found at /doc/proposal/proposal.pdf.

>>>
Controlling humanoid robots to make them walk is a rather complex and difficult task.
Among all the difficulties, one of the most notable ones is that the movement itself is unstable
in its nature since the robot is not fixed to the ground. In each walking cycle there are two moments
in which the robot is standing in just one foot while the other is moving to perform a step.
Several disciplines like Physics, Biology, Mechanics and Computer Science converge in the understanding
of the sequence of movements that lead to a successful and effective gait. There is not a single
universal solution to this challenge. Our proposal for the course's project is to tackle the problem
with evolutionary algorithms.
>>>

Report: https://gitlab.com/sprkrd/ci/raw/master/final_report.pdf

## Instructions

To come...