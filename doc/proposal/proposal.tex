\documentclass[11pt]{article}

\usepackage{hyperref}
\usepackage{tocbibind}
\usepackage{graphicx}

\title{Biped walk with evolutionary algorithms}
\author{Salvador Medina Herrera \and Carles Miralles Pena \and Alejandro Su\'arez Hern\'andez}

\begin{document}

\maketitle

\tableofcontents

\section{Introduction}

Controlling humanoid robots to make them walk is a rather complex and difficult
task. Among all the
difficulties, one of the most notable ones is that the movement
itself is unstable in its nature since the robot is not fixed to the ground. In each
walking cycle there are two moments in which the robot is standing in just one foot
while the other is moving to perform a step. Several disciplines like Physics, Biology,
Mechanics and Computer Science converge in the understanding of the sequence of
movements that lead to a successful and effective gait. There is not a single universal
solution to this challenge. Our proposal for the Computational Intelligence's project
is to tackle the problem with evolutionary algorithms.

\section{Related work}

One can think of finding the position of the motors at each
step of the gait iteration (for example by trial and error). This is the
philosophy that follows the Bioloid educational robot (see figure \ref{fig:bioloid}
and also check \cite{bioloidweb}). This humanoids comes with a set
of default ``pages" each of which represents a different movement.
The main problem with this method is that it is ad-hoc and not very robust when the mass distribution
of the robot is subject to notable changes. In many circumstances, this strategy it is not adaptable enough
(although it is worth mentioning that, in the particular case of the Bioloid, it comes with a gyroscope
and a balancing module that mitigates somewhat this disadvantage).

\begin{figure}[htb]
\centering
\includegraphics[height=4cm]{./robotis-bioloid}
\caption{The Bioloid robot}
\label{fig:bioloid}
\end{figure}

There are other methods that are based on modeling the robot with high accuracy and then
develop a control scheme based on this model and the interaction and constraints between
the different parts of the robot. The work \cite{virtualmodelcontrol} describes a technique called Virtual Model Control
that approaches the problem from this point of view. The method is, therefore, more
robust than the preceding one, although we need a model of the robot. Another
difficulty which is discussed in the dissertation is the non-ideal actuators.

The problem has been tackled from the perspective of Reinforcement Learning too.
\cite{rlearn} is such an example. In this case, the robot learns to walk by means
of repeated experiments
in which the actions that lead to failure (e.g. falling or not meeting another soundness
criteria) are penalized, while the actions that lead to successful behaviour are
rewarded. This method gains in generality and adaptability with respect to the
previous ones, although it has the disadvantage of introducing a learning
stage before the robot can actually walk.

\section{Evolutionary Algorithms in the problem of the humanoid's gait}

Much like in the reinforcement learning case, we would like to have a fair
degree of adaptability and generality.
There are several works with this intention in mind that consider Evolutionary Algorithms
to approach the problem. Examples of such work are \cite{picado2009auto} and \cite{arakawa1996natural}.
They use genetic algorithms.
The basic idea is that the individuals (chromosomes) that form the populations are
the parameters of the curves described by the robot's legs. The fitness function varies
from work to work. For instance, in \cite{picado2009auto} the authors optimize the energy of
the movement
with the hope of achieving a smooth and human-like movement, while in
\cite{arakawa1996natural} the authors propose
to optimize the average oscillation of the torso. Of course, there are plenty more of
works with additional ideas.

\section{Practical concerns}

We can focus the project on the Darwin, a small humanoid robot
(see figure \ref{fig:darwin}).
First we can tackle the problem with a simulator. We can use Gazebo
(link at \cite{gazebo}), which is
a well-known open source simulator. Another option is Webots. This one has a limited free version
which should be enough for our purposes (at \cite{webots}) if we are willing to use it.
Alternatively, we can use The Construct Sim,
\cite{constructsim}, which is a website that allows us to perform online simulations
(with either Gazebo or Webots).

\begin{figure}[htb]
\centering
\includegraphics[height=4cm]{./Darwin_OP}
\caption{The Darwin robot}
\label{fig:darwin}
\end{figure}

If we achieve a satisfactory result inside the simulation and if time allows, we could make a
real Darwin robot walk. The IRI can provide us with one of those.

We can apply one of the approaches suggested in one of the previously cited articles or
any of the many articles that revolve around this matter.
It is worth mentioning that course notes suggest Evolution Strategies
as a stronger option for problems in which there are real numbers involved in the
solution, so an
interesting topic could be extracting the main ideas
of one of \cite{picado2009auto} or \cite{arakawa1996natural}
(or others) and extrapolate them into an Evolution Strategies algorithm.
\cite{esscholar} contains a summary of the theory behind these kind of algorithms, while
\cite{escomprehensive} is more exhaustive
and contains greater details. All in all, the objective would be to successfully manage to
make the robot walk in a simulation environment and (perhaps) in the real world, using
evolutionary algorithms.

\begin{thebibliography}{99}

\bibitem{picado2009auto}
Automatic generation of biped walk behavior using genetic algorithms,
\emph{by Hugo Picado et al}
\url{https://pdfs.semanticscholar.org/c9a4/5e9e076bce190037d21bd8ed62be9e36c61e.pdf}.

\bibitem{arakawa1996natural}
Natural Motion Trajectory Generation of Biped Locomotion Robot using Genetic
Algorithm through Energy Optimization \emph{by Takemasa Arakawa and Toshio Fukuda}
\url{http://ieeexplore.ieee.org/document/571368/}

\bibitem{gazebo}
Gazebo simulator,
\url{http://gazebosim.org/}

\bibitem{webots}
Webots simulator,
\url{https://www.cyberbotics.com/}

\bibitem{constructsim}
The Construct Sim (simulators online!)
\url{http://www.theconstructsim.com/}

\bibitem{html5viz}
Illustration of the main idea \emph{by Rafael Matsunaga}
(who, by the way, is a world yo-yo champion),
\url{http://rednuht.org/genetic_walkers/}

\bibitem{esscholar}
Small article about Evolution strategies at Scholarpedia,
\url{http://www.scholarpedia.org/article/Evolution_strategies}

\bibitem{escomprehensive}
Evolution strategies - A comprehensive introduction,
\emph{by Hans-Georg Beyer and Hans-Paul Schwefel.}
\url{http://www.cs.bham.ac.uk/~pxt/NIL/es.pdf}

\bibitem{rlearn}
Dissertation about biped robots that learn to walk by means of Reinforcement Learning,
\emph{by H. Benbrahim and J.A. Franklin},
\url{http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.99.6229&rep=rep1&type=pdf}

\bibitem{bioloidweb}
Bioloid robot,
\url{http://www.robotis.com/xe/bioloid_en}

\bibitem{virtualmodelcontrol}
Virtual Model Control of a Biped Walking Robot \emph{by Jerry E. Pratt},
\url{ftp://ftp.uk.freesbie.org/sites/www.bitsavers.org/pdf/mit/ai/aim/AITR-1581.pdf}

\end{thebibliography}

\end{document}