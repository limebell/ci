%[ALEJANDRO] He revisado esta sección. He añadido algunos detalles que me han parecido importantes.
\section{Application of CMA-ES to biped-locomotion generation}
\label{sec:cma-es}

For this project, we have decided to use a particular subfamily of Evolution Strategies: Covariance Matrix Adaptation - ES. The pillars that support this decision are mainly three: at the time of writing this report, this algorithm is considered the state-of-the-art of ES (arguably even of Evolutionary Algorithms); the lack of literature on the application of CMA-ES to tackle the generation and optimization of biped locomotion and our genuine interest in discovering how well-suited it is for this particular task; and, in words of their authors, it does not require an intensive parameter tweaking. In section \ref{sec:cma-es} we give more details about how we tackle our goals.

\emph{Covariance Matrix Adaptation - Evolution Strategies}, or simply CMA-ES, were first described in \citep{hansen2001completely}. For the reader's convenience, we have outlined the CMA-ES algorithm (pseudo-code included) in Appendix \ref{sec:cma-es-algorithm}. The new individuals are generated according to a multivariate Gaussian distribution. Part of the success of this method comes from the great power that provides the Covariance Matrix Adaptation scheme to adapt to the fitness function. This adaptation is somewhat reminiscent of the update step in quasi-Newton methods, although derivative free, robust to noise and with potential to avoid getting stuck in local minima.

The algorithm depends on the parameters  $\lambda$, $\mu$, as well as the initial $m$, $\sigma_0$ and $C$. Here $\lambda$ is the size of the population each generation; $\mu$ defines the number of individuals that will be used to calculate the new mean; $\sigma_0$ represents is the ``step'' and modulates the strength of the mutation; and  $C$ is the initial co-variance matrix. Note that $\sigma_0$ and $C$ are updated at each  generation depending on the generated population and that their initial value can be decided (or let to the default).
% [ALEJANDRO] El centroide también se actualiza en cada paso y no por ello es "poco importante" el valor que tenga al principio. En nuestro caso, sigmas iniciales muy grandes (e.g. 0.30) hacen que el robot acabe arrastrándose.
% so fine optimization of these parameters is not needed.

\subsection{Fitness function\label{sec:fitness-function}}
The parents of the next generation are decided depending upon the fitness function.  This function is very important, as it will guide the algorithm towards the  solution that we  are trying to find.

We use the fitness function proposed in \citep{heinen2006applying}:
\begin{equation}
f = \frac{\delta}{1 + \theta}
\label{eq:fitness-function}
\end{equation}
where
\begin{subequations}
\begin{equation}
\theta(r_{1 \dots n}, p_{1 \dots n}, y_{1 \dots n}) =
\sqrt{Var(r_{1 \dots n}) + Var(p_{1 \dots n}) + Var(y_{1 \dots n})}
\end{equation}
\begin{equation}
\delta(y_{left foot}, y_{right foot})  =  \max(y_{left foot}, y_{right foot})
\end{equation}
\end{subequations}

$\delta$ represents the distance traversed by the robot during a single simulation (we simulate 10 seconds). Instead of using the center of gravity or any other part of the body, we use the distance of the furthest foot in good contact with the ground. This will ensure that individuals that fall forwards are not preferred over individuals that barely advance but do not fall. This will also avoid no-biped advancement up to certain extend.

$\theta$ measures the instability of the robot. Namely, it is the average oscillation (taking into consideration its roll, pitch and yaw) of the torso in radians.

Combining $\delta$ and $\theta$ as shown in Equation \ref{eq:fitness-function} we can optimize both the robot's speed and stability avoiding multi-objective fitness function. This way we do not incur on a careful selection of weighting factors for each objective. Moreover for this first take on the problem we want to avoid including very specific knowledge about the problem (like the ZMP in \citep{arakawa1996natural}).

It is important to stress that the fitness function is not deterministic because the simulator has a stochastic behavior\footnote{Simulations in Gazebo are not repeatable: \url{http://answers.ros.org/question/40208/gazebo-simulations-not-repeatable/}}. Therefore the same individual will render different fitness values each time it is simulated. Typically, the deviation is quite slight; but there are cases in which the individuals are at the edge of stability and may either fall or walk at a fast pace.

Therefore, in addition to the fitness function from equation \ref{eq:fitness-function}, we propose an alternative fitness evaluation that emphasizes stability and is robust against randomness. This evaluation can be seen in Algorithm \ref{alg:alt-fitness}. Basically it consists on performing several simulations, picking the worst of the measured fitness (worst-case scenario); and clamping the distance so once the robot reaches $\delta_{max}$, the only way of increasing the fitness function is by reducing the average oscillation.

\IncMargin{2em}
\begin{algorithm}
\SetKwFunction{SimulateIndividual}{SimulateIndividual}
\SetKwFunction{min}{min}
\SetKwInOut{Input}{input}
\SetKwInOut{Output}{output}

\Input{individual I}
\Output{Fitness evaluation}

 \For{$i \leftarrow{1, M}$}{
 	$\delta_i, \theta_i \leftarrow $ \SimulateIndividual{I} \;
    
    $f_i \leftarrow \frac{\min(\delta_{max}, \delta_i)}{1 + \theta_i}$ \;
 }
 \Return{\min{$f_1 \dots f_M$}}
 
 \caption{Alternative fitness evaluation procedure}
 \label{alg:alt-fitness}
\end{algorithm}
\DecMargin{2em}

We apply the fitness function from Equation \ref{eq:fitness-function} in section \ref{sec:starting-static} to generate walking behavior starting from static positions. Then, as we can see in section \ref{sec:starting-dynamic}, we use Algorithm \ref{alg:alt-fitness} as the evaluation method in an improvement phase aimed at generating more stable individuals starting from marginally stable ones.

% Unstable solutions are more prone to fall but might not do it under certain conditions. For this, it is also possible to improve the stability and reduce variance of the solutions if the fitness function is executed more than once per individual and the worst solution is selected. 

% This same fitness function can be used to further optimize stability. If the  simulation is stopped after a certain distance has been traversed instead of waiting until the simulation time has finished,  more stable solutions will be selected while the speed will not be a determinant factor if it exceeds a certain value.

\subsection{Representation of the solutions}
\label{sec:solution-representation}

We apply the idea proposed in \citep{picado2009automatic}. Since joints typically follow a cyclic movement, we can associate a periodic oscillator to each of them. Each oscillator consists in a single term Fourier series (i.e. a constant plus a sinus function), as shown in \ref{eq:oscillator}. Much like in the aforementioned work we assume that all the oscillators share the same period.
\begin{equation}
 f(t) = C + A_1\sin\left(\frac{2\pi}{T}t+\phi_1\right) \forall t \in R
 \label{eq:oscillator}
\end{equation}

\begin{wrapfigure}{l}{0.5\textwidth}
\begin{center}
\includegraphics[width=0.48\textwidth]{img/darwin_joints_named.png}
\end{center}
\caption{This image depicts all the \darwin's joints.}
\label{fig:darwin-joints-named}
\end{wrapfigure}

This means that for defining a solution using this simplification we have to consider 3 variables for each joint: the offset $C$, the amplitude $A$ and the phase $\phi$; as well as the period of the global oscillation $T$.

In the case of the \darwin robot, we would need $N_{joints} \cdot 3+1=61$ parameters to represent the movement of all the joints. We can see in figure \ref{fig:darwin-joints-named} all the robot's joints. Fortunately, the number of parameters can be greatly reduced if we fix the joints that do not have a great influence over the gait (like \emph{j\_elbow}) and we assume sagittal symmetry. 

For the purpose of this work, each individual explicitly defines the movement only for \emph{j\_shoulder\_pitch\_l},  \emph{j\_hip\_roll\_l}, \emph{j\_hip\_pitch\_l}, \emph{j\_knee\_l}, \emph{j\_ankle\_pitch\_l} and  \emph{j\_ankle\_roll\_l}. The right hand side is inferred by symmetry. For this reason, the total number of variables needed for representing a solution is $6 \cdot 3+1=19$ real values.

It is worth mentioning that we define a maximum and minimum limits for each of these 19 parameters to keep the algorithm from wasting time on absurd solutions. If an generated individual violates some of these constraints, we forced it back (reproject) into the feasible space. This is one of the methods studied in \cite{diouane2015globally} for dealing with constrained problems.

\subsection{Termination criteria\label{sec:termination-criteria}}

Problems with a big search space and noisy fitness functions like this one may take a lot of time to converge, or even not converge at all. Furthermore, one of the main goals of our work is to be able to find a solution in  a very small time.  For this reason, it is very important to accurately define termination the termination criteria of the executions.

We have picked four of the criteria described in \citep{hansen2009benchmarking}, further restricting the maximum number of created individuals. The complete list of termination criteria that we have used is listed below:

\begin{itemize}
\item  \textbf{MaxIter.} We have modified the formula from the paper. Considering that a normal laptop computer can execute up to 4 simulations in real time, that each simulation takes up to 10 seconds and we want to limit this time to 3 days, the maximum number of individuals that can be generated is about 100000. For this reason, we will limit the maximum number of iterations to $100000$/$\lambda$.
\item  \textbf{Stagnation.} The median of the 20 newest values is not smaller than the median of the 20 oldest values, respectively, in the two arrays containing the best function values and the median function values of the last $0.2t + 120 + \frac{30D}{\lambda}$ iterations. This criterion never triggered in our case.
\item \textbf{ConditionCov.} The condition number of covariance matrix exceeds $10^{14}$ (inversion becomes numerically unstable).
\item \textbf{EqualFunVals.} In more than $\frac{1}{3}$ of the last D iterations the objective function value of the best and the k-th best solution are identical, that is $f(x_{1 \dots \lambda}) = f(x_{k \dots \lambda})$, where $k = 1 + 0.1 + \frac{\lambda}{4}$ (low diversity).
\end{itemize}

In addition, we have added an additional criterion of our own in the improvement phase described in \ref{sec:starting-dynamic}:
\begin{itemize}
\item \textbf{GoodEnough}: the fitness value of the best individual from last generation exceeds $\delta_{max}/1.2$ (i.e. it reaches the threshold distance with an average oscillation below 0.2 radians).
\end{itemize}

% \subsection{Advantages of CMA-ES for the task of biped walk optimization}
% % TODO: yo este apartado no lo acabo de ver...luego se habla de advantages.. y lo de derivation free, yo no lo entiendo, creo q se debería desarrollar un poco mas para evitar malas interpretaciones..
% % [ALEJANDRO] Estoy de acuerdo. Todo esto se ha cubierto con más brevedad anteriormente. Creo que es mejor recortar.
% CMA-ES has multiple advantages over other optimization algorithms, specially for the type of task that we are facing. Some of these advantages are the following:

% To begin with, they work well with stochastic and noisy problems like this one, as the simulator that we are using is not always deterministic and the motors of the actual robot also have a certain tolerance range.
% %Review??
% Closely related to this, CMA-ES is a derivative-free method and does not require an explicit fitness function as a result. This is also very important because the values that we are using for calculating the fitness of a solution comes from a simulator.

% Third, it does not require fine parameter tuning or optimization as it can self-adapt. This is also very important for our problem, as simulations usually take a lot of time and finding the best set of parameters would take too much time if other kinds of algorithms were used.

%\url{http://ai2-s2-pdfs.s3.amazonaws.com/9d90/0d17ecdbcb9c84eb21f61ec53afe0838a019.pdf}