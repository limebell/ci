#!/usr/bin/python

import pickle
import rospy

from darwin_gazebo.srv import SimulateIndividual, SimulateIndividualRequest
from deap import creator
from deap import base

joint_names = ['j_shoulder_pitch_l', 'j_hip_roll_l', 'j_hip_pitch_l',
  'j_knee_l', 'j_ankle_pitch_l', 'j_ankle_roll_l',
  'j_shoulder_roll_l', 'j_elbow_l']

def evaluateInd(individual):
  request = SimulateIndividualRequest()

  request.joint_names = joint_names
  request.T = individual[0]
  request.A = individual[1:17:3] + [0.0, 0.0]
  request.C = individual[2:18:3] + [0.41, 0.76]
  request.phi = individual[3:19:3] + [0.0, 0.0]
  request.max_duration = 10.0
  print request

  proxy = rospy.ServiceProxy('/sim01/darwin/simulate_individual',
      SimulateIndividual)
  response = proxy(request)
  print response
  return (response.distance / (1+ response.theta),)

creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", list, fitness=creator.FitnessMax, strategy=None)

with open('checkpoint_bst.pkl', 'rb') as f:
    cp = pickle.load(f)

hof = cp["halloffame"]

# hof = [[0.51680752845598588, -0.28436805142800731, -0.82734558092343169,
#   0.25163373816570223, -0.033039108931724875, -0.0078498538619070288,
#   0.2088188106811264, -0.64296489017519753, -0.18115950970409467,
#   -0.37780432212635046, 0.52144808862483905, -0.80228696681374634,
#   -0.10465645371686648, -0.24259780858873972, -0.74288464475137783,
#   -1.1056022752431811, 0.052437558804736981, 0.087713639012615557,
#   -0.786369461552336]]

for individual in hof:
    print individual
    print evaluateInd(individual)
