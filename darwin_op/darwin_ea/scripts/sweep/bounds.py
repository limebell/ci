from math import pi

def checkBounds(min_=None, max_=None):
    """
    Returns decorator that fixes violated range constraints
    """
    min_ = min_ or [0.5,
        -pi, -pi, -pi,
        -0.8, -0.8, -pi,
        -pi/2, -pi/2, -pi,
        -pi/2, -pi/2, -pi,
        -0.8, -0.8, -pi,
        -0.3, -0.25, -pi]

    max_ = max_ or [3.0,
        pi, pi, pi,
        0.8, 0.0, pi,
        pi/2, pi/2, pi,
        pi/2, 0.05, pi,
        0.8, 0.8, pi,
        0.3, 0.3, pi]

    def decorator(func):
        def wrapper(*args, **kwargs):
            offspring = func(*args, **kwargs)
            for child in offspring:
                for idx in xrange(len(child)):
                    child[idx] = max(min_[idx], child[idx])
                    child[idx] = min(max_[idx], child[idx])
            return offspring
        return wrapper
    return decorator
