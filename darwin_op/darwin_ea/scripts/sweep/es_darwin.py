#!/usr/bin/python

#http://deap.gel.ulaval.ca/doc/dev/tutorials/advanced/checkpoint.html
import sys,getopt
import array
import random
import numpy as np
import eval_pool
import bounds
import os
import time

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from math import pi

from deap import algorithms
from deap import benchmarks
from deap import base
from deap import creator
from deap import tools
from deap import cma
import pickle

N = 19

def rounded_list_str(l):
    rounded_str = map(lambda n: "{:.2f}".format(n), l)
    return "[" + ", ".join(rounded_str) + "]"


def print_start_msg(strategy, start_gen, halloffame):
    print """
        ######################
        ######################
        ######################
        #
        # Starting new CMA-ES execution with the following strategy parameters:
        # lambda: {lambda_}
        # mu: {mu}
        # centroid: {centroid}
        # diagD: {diagD}
        # sigma: {sigma}
        #
        # Starting from generation: {start_gen}
        # Best individual found: {best}
        # Best fitness evaluation until now: {best_f}
        #
        ######################
        ######################
    """.format(centroid=rounded_list_str(strategy.centroid),
            lambda_=strategy.lambda_,
            mu=strategy.mu,
            diagD=rounded_list_str(strategy.diagD),
            sigma=strategy.sigma,
            start_gen=start_gen,
            best=rounded_list_str(halloffame[0]) if halloffame else None,
            best_f=halloffame[0].fitness.values[0] if halloffame else None)

def is_stagnation(best_values, median_values, STAGNATION_ITER):
    stagnation = len(best_values) > STAGNATION_ITER and len(median_values) > STAGNATION_ITER and \
            np.median(best_values[-20:]) <= np.median(best_values[-STAGNATION_ITER:-STAGNATION_ITER+20]) and \
            np.median(median_values[-20:]) <= np.median(median_values[-STAGNATION_ITER:-STAGNATION_ITER+20])
    return stagnation

def close_individuals(ind1, ind2):
    diff_attr = reduce(lambda acc, (a,b): acc + abs(a-b), zip(ind1, ind2), 0)
    diff_f = abs(ind1.fitness.values[0] - ind2.fitness.values[0])
    return diff_attr < 2E-2 or diff_f < 1E-2


def algorithm(centroid=[0.0]*N, n_workers=1, lambda_=400, mu=5, sigma=0.1):
    dir_name = "checkpoint_{}_{}_{:.02f}".format(lambda_, mu, sigma)
    checkpoint_name = "{}/{}.pkl".format(dir_name, dir_name)

    creator.create("FitnessMax", base.Fitness, weights=(1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMax, strategy=None)

    if not os.path.isdir(dir_name):
        os.makedirs(dir_name)

    if os.path.isfile(checkpoint_name):
        with open(checkpoint_name, 'rb') as f:
            cp = pickle.load(f)
        elapsed = cp['elapsed']
        strategy = cp['strategy']
        gen = cp["generation"]
        closefunvalues = cp['closefunvalues']
        best_values = cp["best_values"]
        median_values = cp["median_values"]
        halloffame = cp["halloffame"]
        logbook = cp["logbook"]
        np.random.set_state(cp["rndstate"])
    else:
        elapsed = 0
        np.random.seed(42)
        strategy = cma.Strategy(centroid=centroid, sigma=sigma, mu=mu, lambda_=lambda_)
        gen  = 0
        closefunvalues = list()
        best_values = list()
        median_values = list()
        halloffame = tools.HallOfFame(1) # bests of the class
        logbook = tools.Logbook()

    toolbox = base.Toolbox()
    toolbox.register("generate", strategy.generate, creator.Individual)
    toolbox.decorate("generate", bounds.checkBounds())
    toolbox.register("update", strategy.update)
    evaluators = eval_pool.FitnessEvaluatorsPool(n_workers)

    stats = tools.Statistics(lambda ind: ind.fitness.values) # stats to be computed
    stats.register("avg", np.mean)
    stats.register("std", np.std)
    stats.register("min", np.min)
    stats.register("max", np.max)

    # MAXITER = 100 + 50*(N+3)**2 / np.sqrt(lambda_) # original number (from the paper). Perhaps too many for our case...
    MAXITER = 100000 / lambda_
    EQUALFUNVALS_K = int(np.ceil(0.1 + lambda_ / 4.))

    print_start_msg(strategy, gen, halloffame)

    def check_conditions():
        STAGNATION_ITER = int(np.ceil(0.2*gen + 120 + 30.0*N/lambda_))
        conditions['MaxIter'] = gen >= MAXITER
        conditions['Stagnation'] = is_stagnation(best_values, median_values,
                STAGNATION_ITER)
        conditions['EqualFunVals'] = gen > N and sum(closefunvalues[-N:])/float(N) > 1.0/3.0
        conditions['ConditionCov'] = strategy.cond > 1e14

    conditions = dict()
    check_conditions()

    while not any(conditions.values()):
        print '----\ngeneration ', gen

        # Generate a new population and sort it
        population = toolbox.generate()

        # Evaluate and sort the individuals
        start = time.time()
        evaluators.evaluate_population(population)
        elapsed += time.time() - start
        population.sort(key=lambda ind: ind.fitness.values, reverse=True)

        # Update HoF and logbook with the evaluated individuals
        halloffame.update(population)
        record = stats.compile(population)
        logbook.record(**record)

        toolbox.update(population)

        print "Best from this generation: {}. Fitness: {}".format(
                rounded_list_str(population[0]),
                population[0].fitness.values)
        print "Best until now: {}. Fitness: {}".format(
                rounded_list_str(halloffame[0]),
                halloffame[0].fitness.values)

        gen += 1

        # Get the best and median fitness from this population
        best_values.append(population[0].fitness.values[0])
        median_values.append(population[lambda_/2].fitness.values[0])

        # Check whether the best individual from this population is too similar
        # to the Kessim individual
        if close_individuals(population[0], population[EQUALFUNVALS_K]):
            closefunvalues.append(1)
        else:
            closefunvalues.append(0)

        check_conditions()

        # checkpoints
        with open(checkpoint_name, "wb") as f:
            # checkpoint for continuing with the latest generation
            cp = dict(generation=gen,
                    closefunvalues=closefunvalues,
                    best_values=best_values,
                    median_values=median_values,
                    halloffame=halloffame,
                    logbook=logbook,
                    rndstate=np.random.get_state(),
                    strategy=strategy,
                    elapsed=elapsed)
            pickle.dump(cp, f)

        with open(dir_name+"/population_gen_{:04d}.pkl".format(gen), "wb") as f:
            # keep a history of populations and strategies for each
            # generation
            population_cp = dict(population=population, strategy=strategy)
            pickle.dump(population_cp, f)

    print "END EXECUTION. Conditions satisfied:"
    for condition, value in conditions.items():
        if value:
            print "    " + condition

    evaluators.finish()


def parseParams(argv):
    try:
        opts, args = getopt.getopt(argv,"hw:",["checkpoint="])
    except getopt.GetoptError:
        print 'Usage: ./es_darwin.py [-w <number_of_workers>]'
        sys.exit(2)
    n_workers = 1
    for opt, arg in opts:
        if opt == '-h':
            print 'test.py -i <checkpoint_file>'
            sys.exit()
        elif opt == '-w':
            n_workers = int(arg)
    return n_workers

if __name__ == "__main__":
    n_workers = parseParams(argv = sys.argv[1:])

    centroid = [1.5,
            0.0, 0, 0,
            0.0, 0.0, 0,
            0, 0.3, 0,
            0, -0.6, 0,
            0, -0.3, 0,
            0.0, 0, 0]

    algorithm(n_workers=n_workers, lambda_=10, sigma=0.25, mu=5, centroid=centroid)

