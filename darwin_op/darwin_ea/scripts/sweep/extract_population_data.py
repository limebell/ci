#!/usr/bin/python

import sys
import os
import pickle
import re

import numpy as np
import matplotlib.pyplot as plt

# We need to do this in order for the pickle files to be loaded successfully.
from deap import creator
from deap import base
creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", list, fitness=creator.FitnessMax, strategy=None)

def load_population_and_strategy(file_):
    with open(file_, 'rb') as f:
        d = pickle.load(f)
    return d['population'], d['strategy']

def extract_stats(stats, population, strategy):
    fitness = [ind.fitness.values[0] for ind in population]
    stats['min'].append(fitness[-1])
    stats['max'].append(fitness[0])
    stats['median'].append(fitness[len(fitness)/2])
    stats['p25'].append(fitness[len(fitness)/4])
    stats['p75'].append(fitness[3*len(fitness)/4])
    stats['mean'].append(np.mean(fitness))
    stats['std'].append(np.std(fitness))
    stats['sigma'].append(strategy.sigma)
    if stats['best']:
        stats['best'].append(max(fitness[0], stats['best'][-1]))
    else:
        stats['best'].append(fitness[0])

def new_stats_dict():
    keys = ('min', 'max', 'median', 'p25', 'p75', 'mean', 'std', 'sigma',
            'best', 'gen', 'nind')
    stats = {k: list() for k in keys}
    return stats

def execution_stats(folder):
    cache_stats = os.path.split(folder)[-1] + '_stats.pkl'
    if os.path.isfile(cache_stats):
        with open(cache_stats, 'rb') as f:
            stats = pickle.load(f)
        return stats

    POPREGEX = r'population_gen_[0-9]{4}.pkl'
    files = os.listdir(folder)
    files = filter(lambda f: bool(re.match(POPREGEX, f)), files)
    files.sort()
    print files
    stats = new_stats_dict()
    stats['gen'] = range(1, len(files)+1)
    for file_ in files:
        population, strategy = load_population_and_strategy(os.path.join(
            folder, file_))
        lambda_ = len(population)
        extract_stats(stats, population, strategy)
    stats['nind'] = map(lambda g: lambda_*g, stats['gen'])

    # Cache stats
    with open(cache_stats, 'wb') as f:
        pickle.dump(stats, f)

    return stats

def show_stats(stats, clamp=None, xaxis='gen'):
    if clamp is None:
        clamp = len(stats[xaxis])
    plt.figure()
    plt.hold('on')
    l1, = plt.plot(stats[xaxis][:clamp], stats['mean'][:clamp], 'r')
    l2, = plt.plot(stats[xaxis][:clamp], stats['std'][:clamp], 'g')
    l3, = plt.plot(stats[xaxis][:clamp], stats['max'][:clamp], 'b')
    l4, = plt.plot(stats[xaxis][:clamp], stats['min'][:clamp], 'y')
    l5, = plt.plot(stats[xaxis][:clamp], stats['best'][:clamp], 'c')
    plt.legend((l1,l2,l3,l4,l5), ('mean', 'std', 'max', 'min', 'best'),
               loc='lower right')
    plt.xlabel('Generation' if xaxis == 'gen' else 'Number of individuals')
    plt.ylabel('Fitness')
    plt.grid('on')

if __name__ == '__main__':
    try:
        folder = sys.argv[1]
        clamp = eval(sys.argv[2])
    except Exception as e:
        print "Usage: ./extract_population_data population_folder clamp"
        print "E.g. ./extract_population_data checkpoint_800_1_0.10 None"
        print "     ./extract_population_data checkpoint_400_5_0.15 10"
        sys.exit(-1)
    stats = execution_stats(folder)
    show_stats(stats, clamp=clamp)
    show_stats(stats, xaxis='nind', clamp=clamp)
    plt.show()
