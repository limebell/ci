#!/usr/bin/python

import es_darwin

n_workers = 2
centroid = [1.5,
            0.0, 0, 0,
            0.0, 0.0, 0,
            0, 0.3, 0,
            0, -0.6, 0,
            0, -0.3, 0,
            0.0, 0, 0]

for lambda_ in [200,400,800]:
    mu=5
    sigma=0.1
    es_darwin.algorithm(centroid=centroid, n_workers=n_workers,
        lambda_=lambda_, mu=mu, sigma=sigma)


