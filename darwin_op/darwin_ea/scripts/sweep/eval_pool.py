#!/usr/bin/python

import threading
import rospy

from Queue import Queue, Empty

from deap import base

from darwin_gazebo.srv import SimulateIndividual, SimulateIndividualRequest

# An individual consists in a list whose elements represent the following:
# [T,
# j_shoulder_pitch_A, j_shoulder_pitch_C, j_shoulder_pitch_phi,
# j_hip_roll_A, j_hip_roll_C, j_hip_roll_phi,
# j_hip_pitch_A, j_hip_pitch_C, j_hip_pitch_phi,
# j_knee_A, j_knee_C, j_knee_phi,
# j_ankle_pitch_A, j_ankle_pitch_C, j_ankle_pitch_phi,
# j_ankle_roll_A, j_ankle_roll_C, j_ankle_roll_phi]

joint_names = ['j_shoulder_pitch_l', 'j_hip_roll_l', 'j_hip_pitch_l',
  'j_knee_l', 'j_ankle_pitch_l', 'j_ankle_roll_l',
  'j_shoulder_roll_l', 'j_elbow_l']

def evaluate_ind(individual, proxy):
    request = SimulateIndividualRequest()
    request.joint_names = joint_names
    request.T = individual[0]
    request.A = individual[1:17:3] + [0.0, 0.0]
    request.C = individual[2:18:3] + [0.41, 0.76]
    request.phi = individual[3:19:3] + [0.0, 0.0]
    request.max_duration = 10.0

    response = proxy(request)
    return (response.distance / (1+ response.theta),)

def fake_evaluate_ind(individual, proxy):
    from deap.benchmarks import rastrigin
    return (-rastrigin(individual)[0],)

class FitnessEvaluatorsPool:
    """
    A pool of concurrent fitness evaluators. The threads will be continuously
    evaluating all the individuals that are added to an internal queue (via
    the evaluate_population method). After the fitness is evaluated, it is
    automatically added to the fitness.values attribute of each individual.
    Each thread is created with a different calculate_fitness proxy.
    """
    def __init__(self, N):
        self.individuals_ = Queue()
        service_names = ["/sim{:02d}/darwin/simulate_individual".format(i)
                for i in xrange(1, N+1)]
        # for srv in service_names:
            # rospy.wait_for_service(srv, timeout=30)
        proxies = [rospy.ServiceProxy(name, SimulateIndividual)
                for name in service_names]
        self.threads_ = [threading.Thread(target=self.thread_loop_,
            args=(proxy,)) for proxy in proxies]
        self.active_ = True
        for thread in self.threads_:
            thread.daemon = True
            thread.start()

    def thread_loop_(self, proxy):
        while self.active_:
            try:
                individual = self.individuals_.get(timeout=1.0)
                individual.fitness.values = evaluate_ind(individual, proxy)
                self.individuals_.task_done()
            except Empty:
                pass

    def evaluate_population(self, population):
        for individual in population:
            self.individuals_.put(individual)
        self.individuals_.join()

    def finish(self):
        self.active_ = False
        for thread in self.threads_:
            thread.join()


if __name__ == '__main__':
    class F:
        values = ()
        pass
    class L(list):
        def __init__(self, *args):
            list.__init__(self, args)
            self.fitness = F()

    print "Creating FitnessEvaluatorsPool"
    pool = FitnessEvaluatorsPool(5)
    print "Created"
    population = [L(1,2,3), L(2,2,2), L(0,1,1), L(0,0,0)]
    pool.evaluate_population(population)
    for ind in population:
        print ind, ind.fitness.values
    pool.finish()
