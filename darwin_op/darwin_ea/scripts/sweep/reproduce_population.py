#!/usr/bin/python

import sys
import os
import pickle
import re

import numpy as np
import matplotlib.pyplot as plt
import rospy


from darwin_gazebo.srv import SimulateIndividual, SimulateIndividualRequest

# We need to do this in order for the pickle files to be loaded successfully.
from deap import creator
from deap import base
creator.create("FitnessMax", base.Fitness, weights=(1.0,))
creator.create("Individual", list, fitness=creator.FitnessMax, strategy=None)

joint_names = ['j_shoulder_pitch_l', 'j_hip_roll_l', 'j_hip_pitch_l',
  'j_knee_l', 'j_ankle_pitch_l', 'j_ankle_roll_l',
  'j_shoulder_roll_l', 'j_elbow_l']

def evaluateInd(individual):
  request = SimulateIndividualRequest()

  request.joint_names = joint_names
  request.T = individual[0]
  request.A = individual[1:17:3] + [0.0, 0.0]
  request.C = individual[2:18:3] + [0.41, 0.76]
  request.phi = individual[3:19:3] + [0.0, 0.0]
  request.max_duration = 10.0
  print request

  proxy = rospy.ServiceProxy('/sim01/darwin/simulate_individual',
      SimulateIndividual)
  response = proxy(request)
  print response
  return (response.distance / (1+ response.theta),)

def load_population_and_strategy(folder, gen):
    file_ = os.path.join(folder, "population_gen_{:04d}.pkl".format(gen))
    with open(file_, 'rb') as f:
        d = pickle.load(f)
    return d['population'], d['strategy']




if __name__ == '__main__':
    try:
        folder = sys.argv[1]
        generation = int(sys.argv[2])
        k = int(sys.argv[3])
        times = int(sys.argv[4])
    except Exception as e:
        print "Usage: ./reproduce_simulation folder generation k times"
        sys.exit(-1)
    p, _ = load_population_and_strategy(folder, generation)
    for idx in xrange(times):
        print idx, " loop"
        for ind in p[:k]:
            print evaluateInd(ind)
