#include <boost/bind.hpp>
#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/common/common.hh>
#include <ros/ros.h>
#include <darwin_gazebo/InitializeRobot.h>
#include <darwin_gazebo/RobotFeedback.h>

namespace gazebo
{

class DarwinExtra : public ModelPlugin
{
  private:
    physics::ModelPtr model_;
    physics::Link_V links_;
    event::ConnectionPtr updateConnection_;

    // ROS stuff
    ros::NodeHandle* nh_;
    ros::Publisher fb_pub_; // feedback publisher
    ros::ServiceServer init_robot_srv_; // init robot server

    // Last time, previous position and velocity
//     double last_time_;
//     math::Vector3 last_cog_, last_vel_;

    void computeModelCoG(math::Vector3& cog)
    {
      double mass = 0.0;
      cog = 0.0;
      for (int idx = 0; idx < links_.size(); ++idx)
      {
        double l_mass = links_[idx]->GetInertial()->GetMass();
        math::Vector3 l_cog = links_[idx]->GetWorldCoGPose().pos;
        cog += l_mass*l_cog;
        mass += l_mass;
      }
      cog /= mass;
    }

    double getMinimumZ()
    {
      double z_min = 1e12;
      for (int idx = 0; idx < links_.size(); ++idx)
      {
        double z = links_[idx]->GetBoundingBox().min.z;
        if (z < z_min)
        {
          z_min = z;
        }
      }
      return z_min;
    }

    void nullify_twist()
    {
      for (int idx = 0; idx < links_.size(); ++idx)
      {
        links_[idx]->SetWorldTwist(math::Vector3(), math::Vector3());
      }
      model_->SetWorldTwist(math::Vector3(), math::Vector3());
    }

    bool check_foot(const std::string& foot, geometry_msgs::Point& position)
    {
      physics::LinkPtr foot_l = model_->GetLink(foot + "_leg_ankle_roll");
      math::Vector3 axis = foot_l->GetWorldPose().rot.GetYAxis();
      // this is the cosine of the angle between the world's Z axis and the
      // vector normal to the foot's plane
      double cos_vertical = axis.z / axis.GetLength();
      math::Box bb = foot_l->GetBoundingBox();
      math::Vector3 position_ = bb.GetCenter();
      position.x = position_.x;
      position.y = position_.y;
      position.z = position_.z;
      bool good_contact = bb.min.z < 0.0025 and cos_vertical > 0.98;
      return good_contact;
    }

  public:
    virtual void Load(physics::ModelPtr parent, sdf::ElementPtr /*sdf*/)
    {
      if (not ros::isInitialized())
      {
        nh_ = NULL;
        ROS_ERROR("ROS has not been initialized. Cannot load plugin.");
        return;
      }

      nh_ = new ros::NodeHandle;
      std::string topic_name = "darwin/feedback";
      fb_pub_ = nh_->advertise<darwin_gazebo::RobotFeedback>(topic_name, 3);

      model_ = parent;
      links_ = model_->GetLinks();

      init_robot_srv_ = nh_->advertiseService("darwin/init_robot",
          &DarwinExtra::InitRobotCB, this);

      updateConnection_ = event::Events::ConnectWorldUpdateBegin(
          boost::bind(&DarwinExtra::OnUpdate, this, _1));

      ROS_INFO("DarwinExtra plugin loaded");
    }

    void OnUpdate(const common::UpdateInfo info)
    {
      double current_time = info.simTime.sec + info.simTime.nsec/1e9;
//       double dt = current_time - last_time_;

        darwin_gazebo::RobotFeedback fb_msg;
//         math::Vector3 cog, vel, accel;

//         computeModelCoG(cog);
//         fb_msg.cog.x = cog.x;
//         fb_msg.cog.y = cog.y;
//         fb_msg.cog.z = cog.z;
// 
//         vel = (cog - last_cog_) / dt;
//         fb_msg.linear_vel.x = vel.x;
//         fb_msg.linear_vel.y = vel.y;
//         fb_msg.linear_vel.z = vel.z;
// 
//         accel = (vel - last_vel_) / dt;
//         fb_msg.linear_accel.x = accel.x;
//         fb_msg.linear_accel.y = accel.y;
//         fb_msg.linear_accel.z = accel.z;

        math::Quaternion rotation = model_->GetLink("base_link")->
          GetWorldPose().rot;
        fb_msg.rpy.x = rotation.GetRoll();
        fb_msg.rpy.y = rotation.GetPitch();
        fb_msg.rpy.z = rotation.GetYaw();

        math::Vector3 axis = rotation.GetYAxis();
        double cos_vertical = axis.z / axis.GetLength();

        // Do not bother checking if there is a goot contact between the feet
        // and the ground if the angle of the body with respect to the vertical
        // is too big
        if (cos_vertical > 0.707)
        {
          fb_msg.left_foot_contact = check_foot("left", fb_msg.left_foot_pos);
          fb_msg.right_foot_contact = check_foot("right", fb_msg.left_foot_pos);
        }

        fb_msg.header.stamp = ros::Time(info.simTime.sec, info.simTime.nsec);
        fb_pub_.publish(fb_msg);

//         last_time_ = current_time;
//         last_cog_ = cog;
//         last_vel_ = vel;
    }

    bool InitRobotCB(darwin_gazebo::InitializeRobot::Request&  req,
                     darwin_gazebo::InitializeRobot::Response& res)
    {
      if (req.joint_names.size() != req.positions.size() and
          req.joint_names.size() != req.velocities.size() and
          req.joint_names.size() != req.accelerations.size())
      {
        res.successful = false;
        res.status_msg = "joint_names, positions, velocities and "
          "accelerations are required to have the same size";
        return true;
      }

      model_->GetWorld()->SetPaused(true);

      for (int idx = 0; idx < req.joint_names.size(); ++idx)
      {
        physics::JointPtr joint = model_->GetJoint(req.joint_names[idx]);
        if (not joint)
        {
          ROS_WARN("Joint %s does not exist. Skipping...",
              req.joint_names[idx].c_str());
          continue;
        }
        joint->SetAngle(0, req.positions[idx]);
        joint->SetVelocity(0, req.velocities[idx]);
        // ignore accelerations
      }

      // take robot to desired world pose
      math::Vector3 pos(req.pose.position.x, req.pose.position.y,
          req.pose.position.z);
      math::Quaternion rot(req.pose.orientation.w, req.pose.orientation.x,
          req.pose.orientation.y, req.pose.orientation.z);
      if (not req.place_on_foot.empty())
      {
        std::string link_name = req.place_on_foot + "_leg_ankle_roll";
        // ROS_INFO("Making %s horizontal to the ground", link_name.c_str());
        physics::LinkPtr link = model_->GetLink(link_name);
        if (link)
        {
          math::Pose rel_pose = model_->GetLink(link_name)->GetRelativePose();
          rot = rot * rel_pose.rot.GetInverse();
          pos -= rot * rel_pose.pos;
        }
        else
        {
          ROS_WARN("Link %s does not exist. Skipping place on foot...",
              link_name.c_str());
        }
      }
      math::Pose pose(pos, rot);
      model_->SetWorldPose(pose);
      // Set velocities to 0
//       nullify_twist();

      if (not req.place_on_foot.empty())
      {
        // Does not work as intended
        // model_->PlaceOnNearestEntityBelow();
        // So we do it "manually":
        double delta_z = - getMinimumZ();
        pose.pos.z += delta_z;
        model_->SetWorldPose(pose);
      }

      res.successful = true;
      res.status_msg = "robot configuration initialized successfully";

      // give it a litle push
      model_->SetWorldTwist(math::Vector3(0,-0.1,0), math::Vector3());

      model_->GetWorld()->SetPaused(false);

      return true;
    }

    ~DarwinExtra()
    {
      if (nh_ != NULL) delete nh_;
    }

};

GZ_REGISTER_MODEL_PLUGIN(DarwinExtra)
}
