cmake_minimum_required(VERSION 2.8.3)
project(darwin_gazebo)

catkin_python_setup()

# Load catkin and all dependencies required for this package
find_package(catkin REQUIRED COMPONENTS 
  rospy
  std_msgs
  geometry_msgs
  sensor_msgs
  gazebo_ros
  message_generation
  dynamic_reconfigure
)

generate_dynamic_reconfigure_options(
    cfg/DarwinGazebo.cfg
)

#add_message_files(
#  FILES
#)

add_service_files(
  FILES
  SimulateIndividual.srv
)

generate_messages(
  DEPENDENCIES
  std_msgs
)



find_package(Boost REQUIRED COMPONENTS system)
find_package(gazebo REQUIRED)

catkin_package(
  CATKIN_DEPENDS 
    sensor_msgs
    geometry_msgs
    roscpp
    gazebo_ros 
    message_runtime
  DEPENDS 
    gazebo
)

link_directories(${GAZEBO_LIBRARY_DIRS})
include_directories(${Boost_INCLUDE_DIR} ${catkin_INCLUDE_DIRS}
  ${GAZEBO_INCLUDE_DIRS})

#add_library(darwin_extra src/cpp/darwin_extra.cpp)
#target_link_libraries(darwin_extra ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES})
#add_dependencies(darwin_extra darwin_gazebo_gencpp)

add_library(darwin_fourier_controller src/cpp/darwin_fourier_controller.cpp)
target_link_libraries(darwin_fourier_controller ${catkin_LIBRARIES} ${GAZEBO_LIBRARIES})
add_dependencies(darwin_fourier_controller darwin_gazebo_gencpp darwin_gazebo_gencfg)
