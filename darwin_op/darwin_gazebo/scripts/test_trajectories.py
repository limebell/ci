#! /usr/bin/env python

import rospy
import actionlib
import json

from geometry_msgs.msg import PointStamped
from control_msgs.msg import FollowJointTrajectoryAction, FollowJointTrajectoryGoal
from trajectory_msgs.msg import JointTrajectory, JointTrajectoryPoint
from math import pi, sin, cos
from std_srvs.srv import Empty

import SimpleHTTPServer
import SocketServer

from StringIO import StringIO

simulation = "sim01"
# PORT = 8000
# HOST = ""

class Handler(SimpleHTTPServer.SimpleHTTPRequestHandler):

    working = False

    def do_GET(self):
        if self.working:
            self.send_error(503, "Currently bussy, please wait")
        else:
            self.working = True
            result = processAll(None)
            self.working = False
            if result is None:
                # Internal error
                self.send_error(500, "Could not process the request")
            else:
                # Construct a server response.
                self.send_response(200)
                self.send_header('Content-type','application/json')
                self.end_headers()
                io = StringIO()
                json.dump(result, io)
                self.wfile.write(io.getvalue())
        return

def processAll(chromosomes):
    rospy.init_node('follow_joint_trajectory_client')

    # First reset the position
    proxy = rospy.ServiceProxy('/' + simulation + '/gazebo/reset_simulation', Empty)
    proxy()

    # Start the simulation
    proxy = rospy.ServiceProxy('/' + simulation + '/gazebo/unpause_physics', Empty)
    proxy()

    points = []
    def onGravityCenterData(point):
        points.append(point)
    rospy.Subscriber('/' + simulation + '/darwin/gravity_center', PointStamped, onGravityCenterData)


    #############################################
    # Stiff Joint Trajectory
    #############################################
    print ("Moving Darwin with STIFF Trajectory in Joints")

    N = 1000
    f = 0.5
    T = 2

    # f = sin(2*pi*f*t)
    # f' = 2*pi*f * cos(2*pi*f*t)
    # f'' = -(2*pi*f)**2 * sin(2*pi*f*t)

    # Creates the goal to send to the action server.
    goal = FollowJointTrajectoryGoal()
    # Fills in the goal joint trajectory with two joint trajectory points
    goal.trajectory = JointTrajectory()
    goal.trajectory.joint_names = ['j_shoulder_pitch_l', 'j_shoulder_pitch_r',
            'j_ankle_roll_l','j_ankle_roll_r',
            'j_ankle_pitch_l', 'j_ankle_pitch_r',  'j_elbow_l', 'j_elbow_r',
            'j_hip_pitch_l', 'j_hip_pitch_r', 'j_hip_roll_l', 'j_hip_roll_r',
            'j_hip_yaw_l', 'j_hip_yaw_r', 'j_knee_l', 'j_knee_r', 'j_pan',
            'j_shoulder_roll_l', 'j_shoulder_roll_r', 'j_tilt']
    NJ = len(goal.trajectory.joint_names)
    goal.trajectory.points = [JointTrajectoryPoint() for ii in xrange(N)]
    # Filling in the first point
    # It hast to be the current position of the robot
    for ii in xrange(N):
        t = ii*T/float(N)
        goal.trajectory.points[ii].positions = [sin(2*pi*f*t)]*2 + [0.0]*(NJ-2)
        goal.trajectory.points[ii].velocities = [0.0]*NJ
        goal.trajectory.points[ii].accelerations = [0.0]*NJ
        goal.trajectory.points[ii].velocities = [2*pi*f * cos(2*pi*f*t)]*2 + [0.0]*(NJ-2)
        goal.trajectory.points[ii].accelerations = [-(2*pi*f)**2 * sin(2*pi*f*t)]*2+ [0.0]*(NJ-2)
        goal.trajectory.points[ii].effort = [0.0]*NJ
        goal.trajectory.points[ii].time_from_start = rospy.Duration(.005 + t)




    try:

      # Creates the SimpleActionClient, passing the type of the action
      # (FollowJointTrajectoryAction) to the constructor.

      action_client = actionlib.SimpleActionClient('/'+simulation+'/darwin/darwin_controller/follow_joint_trajectory', FollowJointTrajectoryAction)

      # Waits until the action server has started up and started
      # listening for goals.
      action_client.wait_for_server()

      # Cancel goals, just in case
      action_client.cancel_all_goals()

      # Send goal
      action_client.send_goal(goal)

      # Waits until the action has finished or specific time out
      if not action_client.wait_for_result(rospy.Duration.from_sec(60)):
        print "Action did not finished before time out! -> Cancelling all goals!"
        action_client.cancel_all_goals()
        return None
      else:
        print "Action completed"

#         def integrateTrapezoidal(points):
#             sum_x = 0
#             sum_z = 0
#             base_x = points[0].point.x
#             base_z = points[0].point.z
#             previous_point = points[0]
#
#             previous_point.point.x-=base_x
#             previous_point.point.z-=base_z
#
#             for point in points[1:]:
#                 point.point.x-=base_x
#                 point.point.z-=base_z
#
#                 diff_time = (point.header.stamp.secs-previous_point.header.stamp.secs)+(point.header.stamp.nsecs-previous_point.header.stamp.nsecs)/1e9
#
#                 dif_x = (point.point.x-(point.point.x-previous_point.point.x)/2)*diff_time
#                 dif_z = (point.point.z-(point.point.z-previous_point.point.z)/2)*diff_time
#
#                 previous_point=point
#                 sum_x += abs(dif_x)
#                 sum_z += abs(dif_z)
#
#             return (sum_x, sum_z, -points[-1].point.y)
#
#
#         # Calculate the deviation from the points
#         integral = integrateTrapezoidal(points)
#
#         proxy = rospy.ServiceProxy('/'+simulation+'/gazebo/pause_physics', Empty)
#         proxy()
#
#         return integral

    except rospy.ROSInterruptException as e:
      print ("SimpleActionClient did not process goal: " + str(e))
      return None

processAll(None)

# httpd = SocketServer.TCPServer((HOST, PORT), Handler)
# httpd.serve_forever()


