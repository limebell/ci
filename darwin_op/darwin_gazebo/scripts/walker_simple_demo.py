#!/usr/bin/env python

import rospy
from darwin_gazebo.darwin import Darwin


def setWalkVelocities(robots, velocity):
    for robot in robots:
        robot[0].set_walk_velocity(velocity[0]*robot[1],velocity[1]*robot[1],velocity[2]*robot[1])

if __name__=="__main__":
    robots = []
    rospy.init_node("walker_demo")
    
    rospy.loginfo("Instantiating Darwin Client")
    robots.append((Darwin('robot1/darwin/'),0.125))
    robots.append((Darwin('robot2/darwin/'),0.25))
    robots.append((Darwin('robot3/darwin/'),0.5))
    rospy.sleep(1)
 
    rospy.loginfo("Darwin Walker Demo Starting")


    setWalkVelocities(robots,(0.2,0,0))
    rospy.sleep(3)
    setWalkVelocities(robots,(1,0,0))
    rospy.sleep(3)
    setWalkVelocities(robots,(0,1,0))
    rospy.sleep(3)
    setWalkVelocities(robots,(0,-1,0))
    rospy.sleep(3)
    setWalkVelocities(robots,(-1,0,0))
    rospy.sleep(3)
    setWalkVelocities(robots,(1,1,0))
    rospy.sleep(5)
    setWalkVelocities(robots,(0,0,0))
    
    rospy.loginfo("Darwin Walker Demo Finished")
